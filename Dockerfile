# Docker builder for Golang
#############this create a executable file#############
FROM golang

WORKDIR /go/src/github.com/user/app
COPY . .
RUN set -x && \ 
    go get -d -v . && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .
#################### the end ###########################

##########the image can be reduced to a
CMD ["./app"]  
